const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');

app.use(bodyParser.json());


//Importing Routes
app.use('/storeinfo', require('./routes/StoreInfoRoute'));



//Routes
app.get('/', (req,res) => {
    res.send('testing server');
});


//Connect to DB
mongoose.connect(
process.env.DB_CONNECTION,
{ useNewUrlParser: true }, 
() => console.log('connected to DB!')
);


//How to we start Listening the server
app.listen(3000);