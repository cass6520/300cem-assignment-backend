const express = require('express');
const router = express.Router();
const Post = require('../models/StoreInfoModel');


//Get alll the store
router.get('/', async (req, res) => {
    try{
         const posts = await Post.find();
         res.json(posts);
    }catch(err){
        res.json({message: err});
    }
});

//Add a new store
router.post('/',async (req,res) => {
    const post = new Post({
        //English store name
        name_eng: req.body.name_eng,
        //Chinese store name
        name_chin: req.body.name_chin,
        //English address
        address_eng: req.body.address_eng,
        //Chinese address
        address_chin: req.body.address_chin,
        //Latitude of the store
        latitude: req.body.latitude,
        //Longitude of the store
        longitude: req.body.longitude,
        //Photo of the store 
        storephoto: req.body.storephoto
    });
    try{   
    const savedPost = await post.save()
        res.json(savedPost)
    }catch (err) {
       res.json({message: err}); 
    }
});


//Specific store
router.get('/:id', async (req,res) => {
    try {
         const post = await Post.findById(req.params.id);
         res.json(post);
    }catch (err) {
        res.json({message: err});
    }    
});


//Delete store
router.delete('/:id', async (req,res) => {
    try {
    const removedPost = await Post.remove({_id: req.params.id}) 
    res.json(removedPost);
    }catch (err) {
    res.json({message: err});
    }
});


module.exports = router;